import React, { FC } from 'react'
import { PageTitle } from '../../../_metronic/layout/core'
import { BuilderPage } from './BuilderPage'
import {Link} from 'react-router-dom'

const ClientPage: FC = () => {
  return (
    <>
      <PageTitle breadcrumbs={[]}>Cliente 1</PageTitle>
      <div className="card">
        <div className="card-body">
          <table className="align-middle fs-6 gy-5 no-footer table table-row-dashed">
            <thead>
              <tr className="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                <th>Graficas</th>
                <th>Habilitado</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                    <Link to="/dashboard">
                      Grafica Sankey
                    </Link>
                </td>
                <td>
                  <input
                    className='form-check-input w-30px h-20px'
                    type='checkbox'
                    name='notifications'
                  />
                </td>
              </tr>
              <tr>
                <td>
                    Grafica de barras
                </td>
                <td>
                  <input
                    className='form-check-input w-30px h-20px'
                    type='checkbox'
                    name='notifications'
                  />
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

    </>
  )
}

export default ClientPage
