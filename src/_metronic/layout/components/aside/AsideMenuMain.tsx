/* eslint-disable react/jsx-no-target-blank */
import React from 'react'
import { useIntl } from 'react-intl'
import { KTSVG } from '../../../helpers'
import { AsideMenuItemWithSub } from './AsideMenuItemWithSub'
import { AsideMenuItem } from './AsideMenuItem'

export function AsideMenuMain() {
  const intl = useIntl()

  return (
    <>
      <AsideMenuItem
        to='/dashboard'
        icon='/media/icons/duotune/art/art002.svg'
        title={intl.formatMessage({ id: 'MENU.DASHBOARD' })}
        fontIcon='bi-app-indicator'
      />

      <AsideMenuItem
        to='/builder'
        icon='/media/icons/duotune/art/art002.svg'
        title={intl.formatMessage({ id: 'Clientes' })}
        fontIcon='bi-app-indicator'
      />

      <AsideMenuItem
        to='/users-behaviour'
        icon='/media/icons/duotune/art/art002.svg'
        title={intl.formatMessage({ id: 'Comportamiento de usuarios' })}
        fontIcon='bi-app-indicator'
      />

      <AsideMenuItem
        to='/recommendations'
        icon='/media/icons/duotune/art/art002.svg'
        title={intl.formatMessage({ id: 'Recomendaciones' })}
        fontIcon='bi-app-indicator'
      />
      <div className='menu-item'>
        <div className='menu-content'>
          <div className='separator mx-1 my-4'></div>
        </div>
      </div>

    </>
  )
}
