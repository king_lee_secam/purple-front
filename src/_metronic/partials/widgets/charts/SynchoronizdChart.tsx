import React, { useEffect, useRef } from 'react'
import ApexCharts, { ApexOptions } from 'apexcharts'

import Chart from "react-apexcharts";
import { ChartValueType, ChartHorizontalAxisType } from "./chartEnums";

const constructChartOptions = (
    seriesId: string,
    title: string,
    horizontalAxisType: ChartHorizontalAxisType,
    showMarkers: boolean
): ApexOptions => ({
    chart: {
        id: seriesId,
        type: "area",
        group: "group",
        zoom: {
            type: "x",
            enabled: true
        },
        animations: {
            enabled: false
        },
        toolbar: {
            tools: {
                download: false,
                selection: false,
                zoom: false,
                zoomin: false,
                zoomout: false,
                pan: false,
                reset: false  
            },
            autoSelected: "zoom"
        }
    },
    dataLabels: {
        enabled: true
    },
    legend: {
        show: true,
        showForSingleSeries: false
    },
    title: {
        text: title,
        align: "left"
    },
    grid: {
        row: {
            colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
            opacity: 0.5
        }
    },
    yaxis: {
        labels: {
            minWidth: 40
        }
    },
    colors: ['#008FFB'],
    xaxis: {
        type: "datetime",
        labels: {
            show: true,
            datetimeFormatter: {
                year: "yyyy",
                month: "MMM 'yy",
                day: "dd MMM",
                hour: "HH:mm"
            }
        }
    },
    tooltip: {
        x: {
            show: true
        }
    }
});


interface SyncedValueChartSeries {
    values: [number, number][] | number[];
    name: string;
}

interface SyncedValueChartProps {
    series: SyncedValueChartSeries[];
    horizontalAxisType: ChartHorizontalAxisType;
    showMarkers: boolean;
    valueTypes: ChartValueType[];
    title?: string;
}



const SyncedValueChart: React.FC<SyncedValueChartProps> = props => {
    return (
        <>
            {props.series.map(s => (
                <div key={s.name} >
                    <Chart
                        type="area"
                        height="200"
                        options={constructChartOptions(
                            s.name,
                            s.name,
                            props.horizontalAxisType,
                            false
                        )}
                        series={[{ data: s.values, name: s.name }]}
                    />
                </div>
            ))}
        </>
    );
}

export { SyncedValueChart };    export type { SyncedValueChartSeries };

